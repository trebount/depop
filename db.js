// const tablePrefix = "inlovewithy2k";
const tablePrefix = "master";

class DB{
    constructor(){
        const mysql = require('mysql');
        this.con = mysql.createConnection({
            host: "localhost",
            user: "root",
            password: "",
            database: "depop"
        });
    }

    insertItem(values)
    {
        console.log(values);
        var self = this;
        this.con.connect(async function(err) {
            if (err) throw err;
            console.log("Connected!");
            var id = await self.checkProductExist(values.url).catch((err) => {
                console.log(err);
            });

            var sql = "INSERT INTO "+tablePrefix+"_product(`desc`, aliprice, alishipprice, shipinfo, price, listingprice, profit, size, url) "+
            "VALUES (?, ?,?, ?,?, ?,?, ?,?)";
            if(id)
            {
                sql = "UPDATE "+tablePrefix+"_product SET `desc` = ?, aliprice = ?, alishipprice = ?, shipinfo = ?, price = ?, listingprice = ?, profit = ?, size = ?, url = ? WHERE id = ?";
                self.con.query(sql, [values.desc, values.aliprice, values.alishipprice, values.shipinfo, values.price, values.listingprice, values.profit, values.size, values.url, id], function (err, result) {
                    if (err) throw err;
                    console.log("Updated Article " + values.url + " with ID: "+id);
                });
            }
            else
            {
                var sql = "INSERT INTO "+tablePrefix+"_product(`desc`, aliprice, alishipprice, shipinfo, price, listingprice, profit, size, url) "+
                "VALUES (?, ?,?, ?,?, ?,?, ?,?)";
                self.con.query(sql, [values.desc, values.aliprice, values.alishipprice, values.shipinfo, values.price, values.listingprice, values.profit, values.size, values.url], function (err, result) {
                    if (err) throw err;
                    console.log("1 record inserted for Article " + values.url);
                });
            }
          });
    }

    checkProductExist(url)
    {
        var sqlExists = "SELECT id, url FROM "+tablePrefix+"_product where url = ?";
        return new Promise((resolve, reject) => {
            this.con.query(sqlExists, [url], function (err, result) {
                if (err) throw err;
                var row = result[0];
                if(result.length>1)
                {
                    for(var i = 0;i<result.length;i++)
                    {
                        console.log("Duplicate entry for ID: " +result[i].id);
                    }
                }
                else if(result.length)
                {
                    console.log("Article already exists. ID:  " + row.id + ", " + row.url);
                    resolve(row.id);
                }
                else
                {
                    resolve(0);
                }
                
            });
        });
    }
}

module.exports = DB;