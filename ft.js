const fs = require('fs');
const readline = require('readline');

async function processLineByLine() {
  const fileStream = fs.createReadStream('tmpSizes.txt');

  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity
  });
  // Note: we use the crlfDelay option to recognize all instances of CR LF
  // ('\r\n') in input.txt as a single line break.
  var values = [];
  for await (const line of rl) {
    // Each line in input.txt will be successively available here as `line`.
    if(!line.startsWith('#'))
    {
        values.push(line.replace(/\s+/g, ";").split(';'));
    }
  }

  console.log("Insert your sizes you want to display");
  var sizes = ['s', 'm', 'l'];
  if(process.argv[2] !== 'h')
  {
    for(var i = 0;i<values.length;i++)
    {
      // if(inArray(values[i][0], sizes))
      // {
        if(!values[i][0].includes('Size'))
        {
          console.log('Size '+values[i][0].toUpperCase());
          for(var j = 1; j<values[i].length;j++)
          {
              console.log(camelize(values[0][j])+': '+values[i][j]);
          }
          console.log('\n');
        }
        else
        {
          if(values[i][0].includes('cm'))
          {
            console.log('Size Chart in cm:\n');
          }
          else if(values[i][0].includes('in'))
          {
            console.log('Size Chart in inch:\n');
          }
          
        }
          
      // }
      
    }
  }
  else
  {
    for(var i = 0;i<values[0].length;i++)
    {
        // if(!values[0][0].includes('Size'))
        // {
          console.log('Size '+values[0][i].toUpperCase());
          for(var j = 1; j<values.length;j++)
          {
              console.log(values[j][0]+': '+values[j][i]);
          }
          console.log('\n');
        // }
        // else
        // {
        //   if(values[0][i].includes('cm'))
        //   {
        //     console.log('Size Chart in cm:\n');
        //   }
        //   else if(values[i][0].includes('in'))
        //   {
        //     console.log('Size Chart in inch:\n');
        //   }
        // }      
    }
  }
  
}

processLineByLine();

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if((""+haystack[i]).toUpperCase() == (""+needle).toUpperCase()) return true;
    }
    return false;
}

function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(word, index) {
    return index === 0 ? word.toUpperCase() : word.toLowerCase();
  }).replace(/\s+/g, '');
}