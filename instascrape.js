// const { exception } = require('node:console');
const puppeteer = require('puppeteer-core');
var fs = require('fs');

(async () => {
    const browserURL = 'http://127.0.0.1:21222';
    const browser = await puppeteer.connect({browserURL});
    const pages = await browser.pages();
    const page = pages[0];
    await page.evaluate(()=>{console.log('hi')});
    const result = await autoScroll(page);
    console.log(result);
    console.log(result.length);
    var file = fs.createWriteStream('array.txt');
    file.on('error', function(err) { /* error handling */ });
    result.forEach(function(v) { 
        let user = v.substr(0,v.length-1);
        file.write('specificuser'+ user + '\n'); 

    });
    file.end();
})();

async function autoScroll(page){
    return await page.evaluate(async () => {
        var links = [];
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            try
            {
                var mine = document.getElementById('mine');
                console.log(mine);
                if(mine!==null)
                {
                    console.log("found #mine");
                    var timer = setInterval(() => {
                        // var scrollHeight = document.body.scrollHeight;
                        var scrollHeight = mine.scrollHeight;
                        // window.scrollBy(0, distance);
                        mine.scrollBy(0, distance);
                        // document.querySelectorAll('#mine a').forEach((element)=>{console.log(element.getAttribute('href'))});
                        document.querySelectorAll('#mine a').forEach((element)=>{links.push(element.getAttribute('href'));});
                        links = links.filter(function (value, index, array) { 
                            return array.indexOf(value) === index;
                        });
                        totalHeight += distance;
                        console.log(totalHeight);
                        console.log(scrollHeight);
                        if(totalHeight >= scrollHeight){
                            clearInterval(timer);
                            resolve();
                        }
                    }, 100);
                }
                
            }
            catch(e)
            {
                console.log(e);
                throw new exception(e);
            }
            
        });

        return links;
    });
}

async function getActivePage(browser, timeout) {
    var start = new Date().getTime();
    while(new Date().getTime() - start < timeout) {
        var pages = await browser.pages();
        var arr = [];
        for (const p of pages) {
            if(await p.evaluate(() => { return document.visibilityState == 'visible' })) {
                arr.push(p);
            }
        }
        if(arr.length == 1) return arr[0];
    }
    throw "Unable to get active page";
}
