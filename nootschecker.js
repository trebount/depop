const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({headless:false});
    var page = await browser.newPage();
    setInterval(async ()=>{
        await page.goto('https://euro-nootropics.com/products/bromantane?variant=37591548690605', { waitUntil: "networkidle2" });
        var soltOutText = await page.evaluate(() => { return $('#AddToCart').text().trim() });
        console.log(soltOutText);
    }, 10000);
    
})();