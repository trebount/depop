var states = {
  "AL": "Alabama",
  "AK": "Alaska",
  "AS": "American Samoa",
  "AZ": "Arizona",
  "AR": "Arkansas",
  "CA": "California",
  "CO": "Colorado",
  "CT": "Connecticut",
  "DE": "Delaware",
  "DC": "District Of Columbia",
  "FM": "Federated States Of Micronesia",
  "FL": "Florida",
  "GA": "Georgia",
  "GU": "Guam",
  "HI": "Hawaii",
  "ID": "Idaho",
  "IL": "Illinois",
  "IN": "Indiana",
  "IA": "Iowa",
  "KS": "Kansas",
  "KY": "Kentucky",
  "LA": "Louisiana",
  "ME": "Maine",
  "MH": "Marshall Islands",
  "MD": "Maryland",
  "MA": "Massachusetts",
  "MI": "Michigan",
  "MN": "Minnesota",
  "MS": "Mississippi",
  "MO": "Missouri",
  "MT": "Montana",
  "NE": "Nebraska",
  "NV": "Nevada",
  "NH": "New Hampshire",
  "NJ": "New Jersey",
  "NM": "New Mexico",
  "NY": "New York",
  "NC": "North Carolina",
  "ND": "North Dakota",
  "MP": "Northern Mariana Islands",
  "OH": "Ohio",
  "OK": "Oklahoma",
  "OR": "Oregon",
  "PW": "Palau",
  "PA": "Pennsylvania",
  "PR": "Puerto Rico",
  "RI": "Rhode Island",
  "SC": "South Carolina",
  "SD": "South Dakota",
  "TN": "Tennessee",
  "TX": "Texas",
  "UT": "Utah",
  "VT": "Vermont",
  "VI": "Virgin Islands",
  "VA": "Virginia",
  "WA": "Washington",
  "WV": "West Virginia",
  "WI": "Wisconsin",
  "WY": "Wyoming"
};

var all = [];

let oldXHROpen = window.XMLHttpRequest.prototype.open;
window.XMLHttpRequest.prototype.open = function(method, url, async, user, password) {
 

  this.addEventListener('load', function() {

    if(this.responseURL.includes('depop.com/api/v1/receipts'))
      {
        //debugger;
        let shop = window.location.href.split('/')[3];
        let r = JSON.parse(this.responseText);
        if(r.buyer.username)
          {
            let url = "https://www.depop.com/"+shop+"/receipts/"+this.responseURL.split('/')[6];
            let fullinsert = "";
            let insert = 'INSERT INTO '+shop+'_order(orderurl, user, firstname, lastname, shipname, shipaddress, shipcity, shipstate, shippostcode, soldtimestamp, shippingamount, sellershippingamount, paymentfee, itemsamount, depopfee, currency, buyershippingamount, buyerpaymentprovider, buyerpaidamount, shiptolocation, totaltaxamount)';
            let totalTaxAmount = r.paymentDetails.taxSummary?.totalTaxAmount!==undefined ? r.paymentDetails.taxSummary.totalTaxAmount : '';
            let shipToLocation = r.paymentDetails.taxSummary?.shipToLocation!==undefined ? r.paymentDetails.taxSummary.shipToLocation : '';
            let values = "VALUES('"+url+"', '"+r.buyer.username+"', '"+r.buyer.firstName+"', '"+r.buyer.lastName+"', '"+r.shippingAddress.name+"', '"+r.shippingAddress.address+"', '"+r.shippingAddress.city+"', '"+states[r.shippingAddress.state]+"', '"+r.shippingAddress.postcode+"', '"+r.soldTimestamp+"', '"+r.paymentDetails.shippingAmount+"', '"+r.paymentDetails.sellerShippingAmount+"', '"+r.paymentDetails.paymentFee+"', '"+r.paymentDetails.itemsAmount+"', '"+r.paymentDetails.depopFee+"', '"+r.paymentDetails.currency+"', '"+r.paymentDetails.buyerShippingAmount+"', '"+r.paymentDetails.buyerPaymentProvider+"', '"+r.paymentDetails.buyerPaidAmount+"', '"+shipToLocation+"', '"+totalTaxAmount+"');";
            fullinsert += insert + values+';\n';
            all.push(insert + values);
            r.items.forEach((e)=>{
              let producturl = 'https://www.depop.com/products/'+e.slug+'/';
              let insert = 'INSERT INTO '+shop+'_orderarticle(orderurl, producturl, productid, size, itemamount)';
              let values = "VALUES('"+url+"', '"+producturl+"', '"+e.productId+"', '"+e.size+"', '"+e.itemAmount+"');";
              all.push(insert + values);
              fullinsert += insert+values+';\n';
            });
            // console.log(fullinsert);
            console.log(all.join(' '));
            
          }
      }
    
    
  });

   
return oldXHROpen.apply(this, arguments);
};