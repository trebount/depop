const puppeteer = require('puppeteer-extra');
const request_client = require('request-promise-native');


(async () => {
    const picsFolder = './pics/';
    
    // removeBG(picsFolder+'black_M_18.jpg');
    // return;
    const fs = require('fs');
    var images = [];
    new Promise(resolve => {
      fs.readdir(picsFolder, (err, files) => {
      files.forEach(async file => {
        console.log(file);
        images.push(picsFolder+file);
      });
      resolve();
    });}).then(() => {
      if(process.argv[2])
      {
        removeBG(images[process.argv[2]]);
      }
      else
      {
        for(let i = 0; i < images.length; i++)
        {
          setTimeout(()=>{
            removeBG(images[i]);
          },8000*i);
        }
      }
      
    })

    

    // return;
    // Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
    
    
  
    // await browser.close();
    async function removeBG(imgPath, resolve)
    {
      const StealthPlugin = require('puppeteer-extra-plugin-stealth')
      puppeteer.use(StealthPlugin())

      // Add adblocker plugin to block all ads and trackers (saves bandwidth)
      const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')
      puppeteer.use(AdblockerPlugin({ blockTrackers: true }))
      const browser = await puppeteer.launch({headless:false});
      
      const page1 = await browser.newPage();
      await page1.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');
      const result = [];

    
      await page1.goto('https://www.remove.bg/', {
        waitUntil: 'networkidle0',
      });

      const page2 = await initEmailGenerator(browser);

      let email = await generateEmail(page2);

      if(await uploadImg(imgPath, page1))
      {
        // await browser.close();
      }

      await submitSignUp(page1, email, true);


      let success = await page1.evaluate(()=>{
        return $('.lead').text().trim() === "We've sent an email to:";
      });

      await checkSuccess(success, page1, page2, browser);
    }

    async function checkSuccess(success, page1, page2, browser)
    {
      if(success)
      {
        console.log("success");
        await page2.bringToFront();

        const btActivate = 'a.btn.btn-primary.btn-lg';
        await page2.waitForSelector(btActivate);
        let successLink = await page2.evaluate((btActivate)=>{return document.querySelector('a.btn.btn-primary.btn-lg').href;},btActivate);

        // await page2.click(btActivate);
        if(successLink)
        {
          const btDownload = "//button[contains(., 'Download')]";
          await page1.bringToFront();
          await page1.goto(successLink);
          await page1.waitForXPath(btDownload);
          // await page2._client.send('Page.setDownloadBehavior', {behavior: 'allow', downloadPath: './bgpics'});
          await pressButton(btDownload, page1);
          // await browser.close();
          // await page1.click('.mfp-close');
          //logout
        }
      }
      else
      {
        console.log("email blocked");
        await page2.bringToFront();
        email = await generateEmail(page2);
        await submitSignUp(page1,email);
        let newSuccess = await page1.evaluate(()=>{
          return $('.lead').text().trim() === "We've sent an email to:";
        });
        await checkSuccess(newSuccess, page1, page2, browser);
      }
    }

    async function uploadImg(imgPath, page1)
    {
      await page1.bringToFront();

      let buttonSel = '.btn.btn-primary.btn-lg.select-photo-file-btn';
      let imagInput = 'input[name="image[original]"]';
      let btDownloadHD = 'button:contains("Download HD")';
      let btDownload = 'a:contains("Download")';
      await page1.waitForSelector(buttonSel);
      await page1.click(buttonSel);
  
      const elementHandle = await page1.$(imagInput);
      await elementHandle.uploadFile(imgPath).catch((err)=>{
        console.log(err);
      });
  
      await page1.waitForXPath("//button[contains(., 'Download HD')]").catch((err)=>{
        console.log(err);
      });
      let downloadHDdisabled = await page1.evaluate((btDownloadHD,btDownload) => {
        // return $(btDownloadHD).attr('disabled')===undefined ? $(btDownloadHD).click() : $(btDownload).click();
        return $(btDownloadHD).attr('disabled');
      },btDownloadHD,btDownload);

      if(downloadHDdisabled===undefined)
      {
        await pressButton("//button[contains(., 'Download HD')]", page1);

        // await page1.click(btDownloadHD);
      }
      else
      {
        await pressButton("//a[contains(., 'Download')]", page1);
        // await page1.click(btDownload);
        return true;
      }
  
      const btSignUpForFree = "//a[contains(., 'Sign up free')]";
      await page1.waitForXPath(btSignUpForFree);
      await pressButton(btSignUpForFree, page1);
    }

    async function submitSignUp(page, email, firsttime = false)
    {
      await page.bringToFront();
      if(firsttime)
      {
        await page.waitForSelector('#user_email');
        await page.type('#user_email', email);
        await page.type('#user_password', "" + 123456);
        await page.type('#user_password_confirmation', "" + 123456);
        await page.click('#user_terms_of_service');
      }
      else
      {
        await page.waitForSelector('#user_email');
        await page.evaluate(()=>{$('#user_email').val("");});
        await page.type('#user_email', email);
        await page.type('#user_password', "" + 123456);
        await page.type('#user_password_confirmation', "" + 123456);
      }

      const btSignUp = "//button[contains(., 'Sign up')]";
      await pressButton(btSignUp, page);

      // await page1.waitForNavigation({ waitUntil: 'networkidle0' });
    }

    async function pressButton(xpath, page)
    {
      let button = await page.$x(xpath);
      button[0].click();
      console.log("pressed "+xpath);
      try {
        await page.waitForNavigation({ timeout: 2000 });
        // do what you have to do here
      } catch (e) {
          // pressButton(xpath, page);
          console.log('element probably not exists');
      }
    }

    async function initEmailGenerator(browser)
    {
      let page2 = await browser.newPage();
      await page2.goto('https://generator.email/', {
        waitUntil: 'networkidle0',
      });
      return page2;
    }

    async function generateEmail(page)
    {
      await page.bringToFront();
      const btGenerate = "//button[contains(., 'Generate new e-mail')]";
      await pressButton(btGenerate, page);
      let email = await page.evaluate(()=>{return $('#email_ch_text').text();});
      return email;
    }
  })();