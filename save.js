const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const axios = require('axios');
const DB = require('./db.js');
const wordsToReplace = ['Shoe Size:', 'Size:', 'Color:'];

var URL = 'https://www.aliexpress.com/item/4000443635825.html';


String.prototype.replaceArray = function(find, replace) {
    var replaceString = this;
    var regex; 
    for (var i = 0; i < find.length; i++) {
      regex = new RegExp(find[i], "g");
      replaceString = replaceString.replace(regex, replace);
    }
    return replaceString;
  };

  
(async () => {
    // const browser = await puppeteer.launch();
    const browser = await puppeteer.launch({headless:false,defaultViewport: null, 
        args: [ '--start-maximized'
            ]});

    if(process.argv[2] === 'pics')
    {
        URL = process.argv[3];
        if(URL.includes('?'))
        {
            URL = URL.split('?')[0];
        }
        if(/^\d+$/.test(URL))
        {
            URL = "https://www.aliexpress.com/item/"+URL+".html";
        }
        console.log("Getting data from " +URL);

        var getPics = true;
        // URL = "https://www.aliexpress.com/item/"+URL+".html";
    }
    else if(process.argv[2])
    {
        URL = process.argv[2];
        if(URL.includes('?'))
        {
            URL = URL.split('?')[0];
        }
        if(/^\d+$/.test(URL))
        {
            URL = "https://www.aliexpress.com/item/"+URL+".html";
        }
        console.log("Getting data from " +URL);

        if(process.argv[3] === 'pics')
        {
            var getPics = true;
        }
    }
    else{
        console.log("URL hardcoded");
    }
    

    var shipping_charge = 5;
    // var aliData = {
    //     shipprice: 1.5,
    //     price: 10.5
    // };
    if(!getPics)
    {
        var page = await setAli(browser);
    }
    else
    {
        console.log("Getting Pictures from page");
        var page = await browser.newPage();
    }
    try {
        await page.goto(URL, { waitUntil: "networkidle2", timeout: 2000 });
    } catch (e) {
        console.log('timeout after goto');
    }
    var aliData = await goAli(page, getPics);

    if(getPics)
    {
        browser.close();
        return;
    }

    var feeData = await goFees(browser,shipping_charge, aliData.shipprice, aliData.price);
    
    var values = await getValues(aliData, feeData);
    let db = new DB();
    await db.insertItem(values);


    // browser.close();

    function getValues(aliData, feeData)
    {
        return {
            desc: aliData.desc,
            aliprice: aliData.price + ' from ' + aliData.originalPrice, 
            alishipprice: aliData.shipprice, 
            shipinfo: aliData.shipinfo, 
            price: parseFloat(aliData.shipprice)+parseFloat(aliData.price), 
            listingprice: feeData.listingprice, 
            profit: feeData.profit, 
            size: aliData.size , 
            url: URL
        }
    }


    async function goFees(browser, shipping_charge, shipping_cost, item_cost)
    {
        const page = await browser.newPage();
        await page.goto('https://www.finalfees.com/depop', { waitUntil: "networkidle2" });


        await page.evaluate((shipping_charge) => {$('[name="shipping_charge"]').val(shipping_charge)},shipping_charge);
        await page.evaluate((shipping_cost) => {$('[name="shipping_cost"]').val(shipping_cost);},shipping_cost);
        await page.evaluate((item_cost) => {$('[name="item_cost"]').val(item_cost);},item_cost);

        await page.waitForSelector('#main-profit');
        var profit = await page.evaluate(() => {return $('#main-profit').text()});

        var soldPrice = Math.round(shipping_charge+shipping_cost+item_cost);



        return new Promise((resolve, reject) => {
            const interval = setInterval(async function () {
                await setTimeout(async () => {
                    await page.evaluate((soldPrice) => { $('[name="sold_price"]').val(''); }, soldPrice);
                    await page.type('[name="sold_price"]', "" + soldPrice);
                }, 500);
                profit = await page.evaluate(() => { return $('#main-profit').text(); });
                console.log(profit);
    
                if (parseFloat(profit) >= parseFloat(13.5)) {
                    clearInterval(interval);
                    resolve({
                        listingprice: soldPrice,
                        profit: profit
                    });
                }
    
                else {
                    soldPrice++;
                }
            }, 1000, soldPrice, profit);
        });

        
        
    }

    async function setAli(browser)
    {
        const page = await browser.newPage();
        await page.goto(URL, { waitUntil: "networkidle2" });
    
        const cookies = [{
                'name': 'aep_usuc_f',
                'value': 'site=glo&c_tp=USD&x_alimid=3921518233&re_sns=google&isb=y&region=US&b_locale=en_US&ae_u_p_s=0'
            },{
                'name': 'cookie2',
                'value': 'val2'
            },{
                'name': 'cookie3',
                'value': 'val3'
        }];
        
        await page.setCookie(...cookies);

        return new Promise(async (resolve, reject) => {
            var setUS = false;

            while(!setUS)
            {
                await page.reload({ waitUntil: ["networkidle2", "domcontentloaded"] });
                
                const data = await page.evaluate(() => document.querySelector('*').outerHTML);
                const $ = cheerio.load(data);
    
                
                var shippingInfo = $('.product-shipping-info').text();
                console.log(shippingInfo);
                if(shippingInfo.includes('to United States'))
                {
                    setUS = true;
                    resolve(page);
                }
            }

        });
    }

    async function getAllReviewImages(sellerId, productId)
    {
        const url = require('url');
        var imageLinks = [];
        for(var pageNr = 1;true;pageNr++)
        {
            console.log(pageNr);
            const { data } = await axios.get('https://feedback.aliexpress.com/display/productEvaluation.htm',{params: {
                ownerMemberId: sellerId,
                memberType: "seller",
                productId: productId,
                evaStarFilterValue: "all Stars",
                evaSortValue: "sortdefault@feedback",
                i18n: true,
                withPictures: true,
                withPersonalInfo: false,
                withAdditionalFeedback:false,
                onlyFromMyCountry:false,
                isOpened:true,
                jumpToTop:false,
                v: 2,
                page: pageNr,
                // currentPage: 1
            }}).catch((error) => {
                console.log("Faulty Response on Page " + pageNr);
            });
            const $ = cheerio.load(data);
            var images = $('img');
            if(!images.length)
            {
                break;
            }
            var feedback = $('.feedback-item');
            feedback.each((idx, el) => {
                var colorText = $(el).find('.user-order-info span').first().text();
                var color = colorText.replaceArray(wordsToReplace, '').trim();
                var sizeText = $(el).find('.user-order-info span').next().first().text();
                var size = sizeText.includes('Logistics:')===false ? sizeText.replaceArray(wordsToReplace, '').trim() : "";
                var images = $(el).find('img');

                if(process.argv[4] === 'text');
                {
                    console.log($(el).find('.buyer-feedback > span').first().text());
                }
            
                images.each((idx, el) => {
                    imageLinks.push({src: $(el).attr('src'), attr: color+'_'+size});
                });
            
            });
            
        }
        console.log(imageLinks);
        //DOWNLOAD

        var fs = require('fs-extra'),
        request = require('request');

        fs.emptyDirSync('pics', { recursive: true },() => console.log('directory cleared!'))

        var download = function(uri, filename, callback){
        request.head(uri, function(err, res, body){
                request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
            });
        };

        console.log("Downloading "+imageLinks.length+" images");
        imageLinks.forEach((el,idx) => {
            download(el.src, 'pics/' + el.attr + '_'+ idx + '.jpg', function(){
                console.log(idx+' done');
            });
        })
        
    }

    async function goAli(page, getPics)
    {
        // await page.addScriptTag({path: require.resolve('jquery')});
        
        

        await page.waitForFunction('window.runParams!=undefined');
        const sellerId = await page.evaluate((runParams)=> window.runParams.data.descriptionModule.sellerAdminSeq);
        const productId = await page.evaluate((runParams)=> window.runParams.data.descriptionModule.productId);

        if(getPics)
        {
            await getAllReviewImages(sellerId, productId);
            return;
        }

        await page.click('.product-shipping-info');
        const btAliShipping = "//div[contains(., 'AliExpress Standard Shipping')]";
        await page.waitForXPath(btAliShipping);
        await page.addScriptTag({url: 'https://code.jquery.com/jquery-3.2.1.min.js'});
        // await page.waitForSelector('.detail_shipping_panel_apply');
        // await page.click('.logo-cainiao_standard');
        await page.evaluate(() => {
            let alishipping = document.querySelector('.logo-cainiao_standard');
            if(alishipping)
            {
                let div = alishipping.closest('.table-tr');
                $(div).find('input').first().click();
                $('button[ae_button_type="detail_shipping_panel_apply"]').click();
            }
            else
            {
                $('.next-icon.next-icon-close.next-medium.next-dialog-close-icon').click();
            }
            
        });
        // await page.addScriptTag({path: require.resolve('jquery')});
        await page.waitForSelector('.product-shipping-info');

        const data = await page.evaluate(() => document.querySelector('*').outerHTML);
        const $ = cheerio.load(data);

        var shippingInfo = $('.product-shipping-info').text();
        console.log(shippingInfo);
    
        var price = $('span[itemprop="price"]').text();
        price = checkFromTo(price);

        if(isNaN(price))
        {
            price = $('span.uniform-banner-box-price').text();
            price = checkFromTo(price);
        }
        var originalPrice = $('.product-price-original .product-price-value').text();
        if(!originalPrice)
        {
            originalPrice = $('.uniform-banner-box-discounts span').text();
        }
        console.log('price:');
        console.log(price);
        console.log('originalPrice:');
        console.log(originalPrice);
    
        var sizesArr = [];
        var sizes = $('.sku-property-text');
        sizes.each(function(idx,el){
            sizesArr.push($(el).text());
        });
        console.log(sizesArr);
    
        var shippingPrice = $('.product-shipping-price span').text();
        if(shippingPrice.includes('Free Shipping'))
        {
            shippingPrice = 0;
        }
        else
        {
            shippingPrice = parseFloat(shippingPrice.split('$')[1]);
        }
        console.log(shippingPrice);

        var desc = $('.product-title-text').text();
        console.log(desc);

        // var ownerMemberId = $('#ownerMemberId').val();
        // console.log(ownerMemberId);
        // var productId = $('#productId').val();
        // console.log(productId);
        // var scripts = $('script');
        // scripts.each((idx, el) => {
        //     // console.log(idx);
        //     // console.log($(el).html.toString());
        //     if($(el).html.toString().includes("window.adminAccountId="))
        //     {
        //         console.log($(el).html.toString());
        //     }
        // });

        return {
            shipprice: shippingPrice,
            shipinfo: shippingInfo,
            size: sizesArr.toString(),
            originalPrice: originalPrice,
            desc: desc,
            price: parseFloat(price),
        }
        
    }

    function checkFromTo(price)
    {
        if(price.includes('-'))
        {
            price = parseFloat(price.split('-')[1]);
        }
        else
        {
            price = parseFloat(price.split('$')[1]);
        }
        return price;
    }

    async function pressButton(xpath, page)
    {
      let button = await page.$x(xpath);
      button[0].click();
      console.log("pressed "+xpath);
      try {
        await page.waitForNavigation({ timeout: 2000 });
        // do what you have to do here
      } catch (e) {
          // pressButton(xpath, page);
          console.log('element probably not exists');
      }
    }

    

})();