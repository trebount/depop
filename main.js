const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const fs = require('fs');

const URL = 'https://www.aliexpress.com/item/4000443635825.html';

(async () => {
    // const browser = await puppeteer.launch();
    const browser = await puppeteer.launch({headless:false,defaultViewport: null, 
        args: [ '--start-maximized',
                '--disable-web-security',
                '--disable-features=IsolateOrigins,site-per-process',
                '--disable-features=site-per-process'
            ]});
    const page = await browser.newPage();
    const userAgent = 'Mozilla/5.0 (X11; Linux x86_64)' +
    'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
    await page.setUserAgent(userAgent);
    await page.goto(URL, { waitUntil: "networkidle2" });

    //click feedback button
    await page.waitForSelector('[ae_object_type="feedback"]').then(() => 
    {
        console.log('got it');
        page.evaluate(()=>document.querySelector('[ae_object_type="feedback"]').click());
    });

    //scroll to feedback
    const feedback = await page.$$('[ae_object_type="feedback"]');
    feedback[1].hover();
    

    //get iFrame
    await page.waitForSelector('#product-evaluation');
    const elementHandle = await page.$('#product-evaluation');
    const frame = await elementHandle.contentFrame();

    await frame.waitForSelector('input#cb-withPictures-filter').then(() => 
    {
        console.log("got frame button");
        // frame.$('input#cb-withPictures-filter').click();
        // console.log(frame.evaluate(()=>document.querySelector('#cb-withPictures-filter')));
        // frame.evaluate(()=>document.querySelector('input#cb-withPictures-filter').click());
        // frame.evaluate(()=>document.querySelector('#cb-withPictures-filter').parentElement.click());
        // frame.evaluate(()=>document.querySelector('#cb-withPictures-filter').setAttribute('checked', 'checked'));
    });



    var label = await frame.$('input#cb-withPictures-filter');
    label.click();
    // var label = await frame.evaluate(()=>document.querySelector('input#cb-withPictures-filter').parentElement);
    console.log('checked');
    // await frame.waitForNavigation({
    //     waitUntil: 'networkidle2',
    //   });

    // await frame.evaluate(()=>document.querySelector('input#cb-withPictures-filter').click());
    // console.log('clicked');

    // await page.waitForSelector('#cb-withPictures-filter').then(() => 
    // {
    //     console.log('got it');
    //     // page.waitForNavigation();
    //     // page.click('[ae_object_type="feedback"]');
    //     page.evaluate(()=>document.querySelector('#cb-withPictures-filter').parentElement.click());
    // });

    // await page.click('[ae_object_type="feedback"]');

    // const feedbackTab = await Promise.race([
    //     page.waitForSelector('[ae_button_type="feedback]"')
    // ]);

    // await page.click(feedbackTab._remoteObject.description);
    // await page.waitForSelector('.tab-item');
    // await page.click('[ae_object_type="feedback"]');



    await page.addScriptTag({path: require.resolve('jquery')});

    const data = await page.evaluate(() => document.querySelector('*').outerHTML);
    const $ = cheerio.load(data);

    // $('[ae_button_type="feedback]"').click();
    // $('.tab-item')[1].click();


    // var imageCount = $('.buyer-review img').length;
    // console.log(imageCount);
    // browser.close();

    async function clickOnElement(elem, x = null, y = null) {
        const rect = await page.evaluate(el => {
          const { top, left, width, height } = el.getBoundingClientRect();
          return { top, left, width, height };
        }, elem);
    
        // Use given position or default to center
        const _x = x !== null ? x : rect.width / 2;
        const _y = y !== null ? y : rect.height / 2;
    
        await page.mouse.click(rect.left + _x, rect.top + _y);
      }
})();