const puppeteer = require('puppeteer-extra');
const cheerio = require('cheerio');
var fs = require('fs');
const shopId = 'goth';
const request_client = require('request-promise-native');
const username = "sexyjillcollections";
const btnSell = '.styles__NavLink-hnl163-3';
const btnLogin = '.styles__LoginButton-cxin5w-5';
const btnCookie = '.sc-gZMcBi.sc-gqjmRU';
const btnWomenswear = '.styles__Link-sc-10mkzda-1';
const btnAppleLink = '.Link-sc-1urid-0';
const btnGetStarted = '.Button__GenericButton-sc-1js6kc1-0';
const lbTitleStyle = '.styles__Title-hh4l5j-8';

(async () => {

    var products = await getProducts(shopId);
    const StealthPlugin = require('puppeteer-extra-plugin-stealth')
    puppeteer.use(StealthPlugin())
    const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')
    puppeteer.use(AdblockerPlugin({ blockTrackers: true }))
    const browser = await puppeteer.launch({headless:false});

    const page = await browser.newPage();
    await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');
    await page.goto('https://www.depop.com/login/');
    await page.click(btnCookie);

    await page.type('#username', username);
    await page.type('#password', 'x0XR!PQm9');
    await page.click(btnLogin);
    //wait for redirect 
    await page.waitForSelector(btnGetStarted);
    const cookies = [{
            'name': 'location',
            'value': 'us'
        },{
            'name': 'cookie2',
            'value': 'val2'
        },{
            'name': 'cookie3',
            'value': 'val3'
    }];

    await page.setCookie(...cookies);
    await page.waitForSelector(lbTitleStyle);
    await page.waitForNavigation({ waitUntil: 'networkidle0' });
    await page.click(btnSell);

    await page.waitForSelector('.listingSelect__value-container');
    let ddLength = await page.evaluate(()=>{
        let dropdowns = document.querySelectorAll('.listingSelect__value-container');
        for(let i = 0;i<dropdowns.length;i++)
        {
            dropdowns[i].setAttribute('id', 'dd'+i);
        }
        return dropdowns.length;
    });
    var data = fs.readFileSync('prod.json', 'utf8');
    for(let i = 0;i<ddLength;i++)
    {
        await page.click('#dd'+i);
        await page.waitForSelector('.listingSelect__option');
        let ddSelector = await selectFromDropdown('Outerwear');
        await pressButton(ddSelector, page);
    }
    
    // await page.waitForSelector('.listingSelect__menu');
    

    // let $ = await getPage(page);
    // ul[@class='featureList' and contains(li, 'Model')]
    
    // await page.waitForXPath(categoryOption);
    
    // await page.click(btnSell);
    // await page.goto('https://www.depop.com/products/create/');

    async function pressButton(xpath, page)
    {
      let button = await page.$x(xpath);
      console.log(button);
      button[0].click();
      console.log("pressed "+xpath);
      try {
        await page.waitForNavigation({ timeout: 2000 });
        // do what you have to do here
      } catch (e) {
          // pressButton(xpath, page);
          console.log('element probably not exists');
      }
    }

    async function getPage(page)
    {
        const data = await page.evaluate(() => document.querySelector('*').outerHTML);
        return cheerio.load(data);
    }

    async function selectFromDropdown(selector)
    {
        let val = `//div[contains(concat(' ',normalize-space(@class),' '),' listingSelect__option ') and contains(., '${selector}')]`;
        console.log(val);
        return val;
    }
})();